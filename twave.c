#include <stdlib.h>

#include <termbox.h>

int
main(void)
{
  struct tb_event  event;

  tb_init();

  for (;;) {
    tb_poll_event(&event);

    if (event.ch == 'Q') {
      break;
    } else {
      tb_printf(0, 0, TB_GREEN, 0, "event: %c", event.ch);
      tb_present();
    }
  }

  tb_shutdown();
  return EXIT_SUCCESS;
}
