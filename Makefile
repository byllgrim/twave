CFLAGS = -std=c89 -pedantic -Wall -Wextra
LDFLAGS = -static -ltermbox

twave: twave.c
	${CC}  ${CFLAGS}  *.c  -o $@  ${LDFLAGS}
