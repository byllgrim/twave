module clknrst;

  initial begin
    $dumpfile("trace.vcd");
    $dumpvars();
  end

  reg clk;
  reg rst;

  initial begin
    clk = 1;
    forever #5 clk = !clk;
  end

  initial begin
    rst = 0;

    #1;
    rst = 1;

    @(posedge clk);
    #1;
    rst = 0;
  end

  initial begin
    repeat (10) @(posedge clk);
    $finish;
  end

endmodule
